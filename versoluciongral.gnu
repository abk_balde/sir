set xla 't'; 
set yla 'State'; 

load 'vars.gnu'

title=sprintf('S');
do for[i=1:M]{title=title."E";}
do for[i=M+1:N] {title=title."I";}
title=title."R"
title=title.sprintf(", {/Symbol b}=%1.2f",beta);
title=title.sprintf(", {/Symbol g}=%1.2f",gamma);
title=title.sprintf(", {/Symbol n}=%1.2f",nu);
title=title.sprintf(", {/Symbol m}=%1.2f",mu);
title=title.sprintf(", N=%d, M=%d",N,M);
title=title.sprintf(", R0 = %f",R0);

set tit title

#set log y 

#Raprox(t)=a*(b-1+c*tanh(c*gamma*t/2.0-d))
#fit Raprox(x) "results.dat" u \
#1:(column(N+3)) via a, b, c, d

set term qt 0
plot "results.dat" u 1:2 w lp t 'S', \
for[f=2+1:N+2] '' u 1:(column(f)) w lp t 'I'.(f-2), \
'' u 1:(column(N+3)) t 'R' w lp,\
'' u 1:(column(N+4)) t 'Sum Infected',\
'' u 1:(column(N+5)) t 'Sum Exposed'

#,\
# Raprox(x) lw 4 t 'Epidemic Curve'
#print "fitted R0= ", 1/(b*a)  #a=1/R0^2 S0, b=S0*R0 => b*a=1/R0; 

set term qt 1
set key outside
set yla 'Total Infected->Total Exposed->Susceptible->Recovered'
plot [][0:1] "results.dat" \
u 1:(column(N+4)):(0) t 'Sum Infected' w filledc,\
'' u 1:(y=column(N+5)+column(N+4)):(y-column(N+5)) t 'Sum Exposed' w filledc,\
'' u 1:(y=$2+column(N+5)+column(N+4)):(y-$2) w filledc t 'S', \
'' u 1:(y=$2+column(N+5)+column(N+4)+column(N+3)):(y-column(N+3)) t 'R' w filledc

#'' u 1:($2+column(N+5)+column(N+4)+column(N+3)):($2+column(N+5)+column(N+4)) t 'R' w filledc

set term qt 2
set key outside
set yla 'Inf.Classes->Exp.Classes->Susceptible->Recovered'

comando=sprintf("< gawk '{if(NR>1){printf(\"%f\",$1); x=0.0;for(i=NF-3;i>=3;i--){x=x+$i; printf(\" %f\",x);}; x=x+$2;printf(\" %f\",x);j=NF-2; x=x+$j; printf(\" %f\",x);print; } }' results.dat");

#system(comando);

#etiqueta(i)=(i-1<=N-M)?(sprintf("I_",i)):((i-1==N+1)?("E"):("R,S"));
#set key lmargin
plot [][0:1] for[i=1+1:1+(N+2)] comando u 1:(column(i)):((i>=3)?(column(i-1)):(0.0)) w filledc t ''

pause -1 "\nPresione Enter Aqui para Terminar\n"







#######################################################################
### coments
# antes:
#plot '< ./sir1 1.0 0.2 0.0 0.0 100' u 1:2 w lp t 'S', \
#for[f=2+1:N+2] '' u 1:(column(f)) w lp t 'I'.(f-2), \
#'' u 1:(column(N+3)) t 'R' w lp, \
#'' u 1:(column(N+4)) t 'Sum Infected',\
#'' u 1:(column(N+5)) t 'Sum Exposed'
#EpidemicCurve(t,g,a,R0,phi)=(g*a**2*0.5/(R0**2))/cosh(a*g*t/2-phi)**2

# para hacer filledcurves...
#gawk '{if(NR>1){printf("%f",$1); x=0.0; for(i=NF-2;i>=2;i--){x=x+$i; printf(" %f",x);}; print; } }' results.dat  > caca.dat

