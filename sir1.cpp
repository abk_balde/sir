/*
====
Solves the model

S => I1 -> I2 -> ... -> IM -> ... -> IN -> R

dS/dt = -beta*S*InfectedGroup - mu*S
dI_1/dt = beta*S*InfectedGroup - gamma*N*I_1 - mu*I_1
dI_i/dt = gamma*N*I_{i-1} - gamma*N*I_i - mu*I_i
dR/dt = GAMMA*I_N - mu*R

InfectedGroup =  I_{M+1} + I_2 + ... + I_N
ExposedGroup = I_1 + I_2 + ... + I_M


Ref.
Modeling Infectious Diseases IN HUMANS AND ANIMALS
Matt J. Keeling and Pejman Rohani
2008 by Princeton University Press

Ecuaciones del modelo:
3.3.1. SEIR and Multi-Compartment Models, pag 94, eq 3.13

TODO: 
* Desagregar en grados de una red scale-free.
* Desagregar en edades.
* Paralelizar usando Thrust.
*/


#include <iostream>
#include <fstream>
#include <boost/array.hpp>
#include <boost/numeric/odeint.hpp>
#include <assert.h>
#include <cstdlib>

using namespace std;
using namespace boost::numeric::odeint;

/////////////////////////////////////////////////////////////
double Dt;

#ifndef NN
#define N	1
//#define NN	2
#endif

#ifndef MM
#define M	0
//#define MM	1
#endif

typedef boost::array< double , NN+2 > state_type;

void sir( const state_type &x , state_type &dxdt , double t )
{
    // x[0] = S == susceptible
    // x[1],x[2],...,x[M] = I_1, I_2,..., IM == exposed 
    // x[M+1],x[2],...,x[N] = I_1, I_2,..., IN == infected
    // x[N+1]= R == recovered

    // InfectedGroup = 	I_{M+1} + ... + I_N = poblaciones contagiosas
    double InfectedGroup=0.0;
    for(int i=MM+1;i<=NN;i++) InfectedGroup+=x[i];

    // dS/dt = -beta*S*InfectedGroup - mu*S
    dxdt[0] = -BETA*x[0]*InfectedGroup - MU*x[0] + NU*x[0]; 			

    // dI_1/dt = beta*S*InfectedGroup - gamma*N*I_1 - mu*I_1
    dxdt[1] =  BETA*x[0]*InfectedGroup - GAMMA*NN*x[1] -MU*x[1]; 	

    // dI_i/dt = gamma*N*I_{i-1} - gamma*N*I_i - mu*I_i
    for(int i=2;i<=NN;i++)
    dxdt[i] =  GAMMA*NN*x[i-1] - GAMMA*NN*x[i] - MU*x[i]; 		

    // dR/dt = GAMMA*I_N - mu*R
    dxdt[NN+1] = GAMMA*NN*x[NN] - MU*x[NN+1]; 				
}

ofstream resout;
void write_sir( const state_type &x , const double t )
{
    double InfectedGroup=0.0;
    for(int i=MM+1;i<=NN;i++) InfectedGroup+=x[i];

    double ExposedGroup=0.0;
    for(int i=1;i<=MM;i++) ExposedGroup+=x[i];

    resout << t;
    for(int i=0;i<NN+2;i++) resout  << '\t' << x[i];
    resout << '\t' << InfectedGroup;
    resout << '\t' << ExposedGroup << endl;
}


int naiveSIR(double trun){

    Dt=0.01;	
 
    resout.open("results.dat");
    resout << "# S, E1,..., EM, IM+1,...,IN, R, SumIi, SumEi" << std::endl;	
	
    ofstream varout("vars.gnu");	
    varout << "N=" << NN << "; M=" << MM << "; beta=" 
	<< BETA << "; gamma=" << GAMMA << "; nu=" 
	<< NU << "; mu=" << MU << "; trun=" 
	<< trun << std::endl;

    // initial conditions (S[0],I[0],R[0])
    state_type x; 
    double initialI=0.00001;	
    double initialS=1-initialI;	
    x[1]=initialI; // == I1 (first exposed class)
    x[0]=initialS; // == S (all susceptibles)
    for(int i=2;i<=NN+1;i++) x[i]=0.0; // == I2,...,INN, R (rest of the world)	

    // integracion 
    integrate( sir , x , 0.0 , trun , Dt , write_sir );

    // basic reprduction ratio (reached if trun long enough)
    varout << "R0 = "<< -log(x[0]/initialS)/x[NN+1] << std::endl;	

    return 0;
}

int main(int argc, char **argv)
{
    assert(argc==2);
    double trun=atof(argv[1]);

    naiveSIR(trun);    	

    return 0;
}

