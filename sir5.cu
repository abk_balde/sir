/*
====
Solves the model

S => E -> {Is,Ia} -> R

InfectedGroup_s=Is	
InfectedGroup_a=Ia	

dS/dt = -S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Mu*S + Nu
dE/dt = S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Alpha*E - Mu*E
dI_s/dt = frac*Alpha*E - Gamma*I_s - Mu*I_s 
dI_a/dt = (1-frac)*Alpha*E - Gamma*I_a - Mu*I_a 
dR/dt = Gamma*(I_a+I_s) - Mu*R 

S ---> E ----> Ia ------> R
	 `---> Is ----'
		`-------> M

frac is the E->Is sintomatic fractions.
(1-frac) is the E->Ia asintomatic fraction.

Alpha=incubation rate
Gamma=recovery rate
Beta_s=sintomatic transmission rate
Beta_a=asintomatic transmission rate
Mu=death rate
Nu=death rate

*/

#if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
#include "gpu_timer.h"
#endif

#include "cpu_timer.h"


#include <iostream>
#include <fstream>
#include <boost/array.hpp>
#include <assert.h>
#include <cstdlib>
#include <vector>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>

#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <boost/numeric/odeint/integrate/integrate_const.hpp>
#include <boost/numeric/odeint/external/thrust/thrust.hpp>

using namespace std;
using namespace boost::numeric::odeint;

/////////////////////////////////////////////////////////////
double Dt;

//typedef boost::array< double , NN+2 > state_type;
typedef double value_type;
typedef thrust::device_vector<value_type> state_type;
typedef state_type::const_iterator const_it_state_type;
typedef state_type::iterator it_state_type;

ofstream varout("vars.gnu");	
ofstream resout("results.dat");

class sir_system{
	private:
	const_it_state_type S_begin, S_end;		
	const_it_state_type E_begin, E_end;		
	const_it_state_type Is_begin, Is_end;		
	const_it_state_type Ia_begin, Ia_end;		
	const_it_state_type R_begin, R_end;		
	const_it_state_type M_begin, M_end;		

	int NE,NI; // chain lengths 
	value_type Beta_s, Beta_a, Alpha, Gamma, frac, Mu, Nu;	
	value_type Trun;
	value_type initialS;
	value_type frac_s, frac_a, fracM;

	public:
	value_type totalS(){
	    	double SuceptibleGroup=thrust::reduce(S_begin,S_end);
		return SuceptibleGroup;
	}
	value_type totalIs(){
	    	double InfectedGroup_s=thrust::reduce(Is_begin,Is_end);
		return InfectedGroup_s;	
	}
	value_type totalIa(){	
	    	double InfectedGroup_a=thrust::reduce(Ia_begin,Ia_end);	
		return InfectedGroup_a;
	}
	value_type totalE(){
	    	double ExposedGroup=thrust::reduce(E_begin,E_end);
		return ExposedGroup;
	}
	value_type totalR(){
	    	double RecoveredGroup=thrust::reduce(R_begin,R_end);
		return RecoveredGroup;
	}
	value_type totalM(){
	    	double DeadGroup=thrust::reduce(M_begin,M_end);
		return DeadGroup;
	}

	sir_system(const state_type &x,  
	value_type Beta_s_, value_type Beta_a_, value_type Alpha_, 
	value_type Gamma_, value_type frac_,value_type fracM_, value_type Mu_, 
	value_type Nu_, int NE_, int NI_, value_type TRUN_):
	Beta_s(Beta_s_),Beta_a(Beta_a_),Alpha(Alpha_),Gamma(Gamma_),
	frac(frac_),fracM(fracM_),Mu(Mu_),Nu(Nu_),NE(NE_),NI(NI_),Trun(TRUN_)
	{
		S_begin=x.begin(); S_end=S_begin+1; 	//S
		E_begin=S_end; E_end=E_begin+NE; 	//SE
		Is_begin=E_end; Is_end=Is_begin+NI; 	//SEIs
		Ia_begin=Is_end; Ia_end=Ia_begin+NI; 	//SEIsIa
		R_begin=Ia_end; R_end=R_begin+1; 	//SEIsIaR	
		M_begin=R_end; M_end=M_begin+1; 	//SEIsIaRM	
		
		initialS=x[0];	

		frac_s=frac;
		frac_a=(1.0-frac_s);

    		varout 
		<< "beta_s=" << Beta_s << "; beta_a=" << Beta_a 
		<< "; alpha=" << Alpha << "; gamma=" << Gamma 
		<< "; fracs=" << frac_s << "; fracM=" << fracM << "; nu=" << Nu 
		<< "; mu=" << Mu << "; trun=" << Trun 
		<< "; NE=" << NE << "; NI=" << NI 
		<< std::endl;
	}

	void operator()( const state_type &x , state_type &dxdt , const value_type t )
	{
	    // x[0] = S == susceptible
	    // x[1],x[2],...,x[M] = I_1, I_2,..., IM == exposed 
	    // x[M+1],x[2],...,x[N] = I_1, I_2,..., IN == infected
	    // x[N+1]= R == recovered
	    // x[N+2]= M == recovered

	    // mapping iterators to classes
	    it_state_type dSdt_begin=dxdt.begin(); it_state_type dSdt_end=dSdt_begin+1; //S
	    it_state_type dEdt_begin=dSdt_end; it_state_type dEdt_end=dEdt_begin+NE; //SE
	    it_state_type dIsdt_begin=dEdt_end; it_state_type dIsdt_end=dIsdt_begin+NI; //SEIs
	    it_state_type dIadt_begin=dIsdt_end; it_state_type dIadt_end=dIadt_begin+NI;//SEIsIa
	    it_state_type dRdt_begin=dIadt_end; it_state_type dRdt_end=dRdt_begin+1;//SEIsIaR		
	    it_state_type dMdt_begin=dRdt_end; it_state_type dMdt_end=dMdt_begin+1;//SEIsIaRM		

	    ////////////////////////////////////////////////////////	    
	    // dS/dt = -S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s)

	    value_type InfectedGroup_s=thrust::reduce(Is_begin,Is_end);//=*Is_begin; 	
	    value_type InfectedGroup_a=thrust::reduce(Ia_begin,Ia_end);//=*Ia_begin; 	

	    #ifdef DEBUGPRINT
	    std::cout << "dS/dt..." << std::flush;		
	    #endif	

	    value_type S=*S_begin;	    		 			
	    *dSdt_begin = -S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s); 			

	    #ifdef DEBUGPRINT
	    std::cout << "done" << std::endl;	
	    #endif	
	
	    ////////////////////////////////////////////////////////
	    // dE/dt = S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Alpha*E 

	    #ifdef DEBUGPRINT
	    std::cout << "dE1/dt..." << std::flush;		
	    #endif	

	    value_type E=*E_begin;	    		 			
	    *dEdt_begin =  S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - NE*Alpha*E; 	

	    #ifdef DEBUGPRINT
	    std::cout << "done" << std::endl;	
	    #endif	
	
	    // cadena de ecuaciones E
	    #ifdef DEBUGPRINT
	    std::cout << "dE[2:NE]/dt..." << std::flush;		
	    #endif	

	    using namespace thrust::placeholders;
	    thrust::transform(E_begin+1,E_begin+NE,E_begin,dEdt_begin+1,
	    NE*Alpha*_2-NE*Alpha*_1);	
	    E=*(E_end-1); //last E	    		 			

	    #ifdef DEBUGPRINT
	    std::cout << "done" << std::endl;	
	    #endif	

	    ////////////////////////////////////////////////////////
	    // dI_s/dt = frac*Alpha*E - Gamma*I_s 

	    #ifdef DEBUGPRINT
	    std::cout << "dIs[1]/dt..."<< std::flush;	
	    #endif	

	    // first Is[1] receives from last E 
	    value_type Is=*Is_begin;	    		 			
	    *dIsdt_begin =  frac_s*Alpha*NE*E -Mu*NI*Is; 	

	    #ifdef DEBUGPRINT
	    std::cout << "done" << std::endl;	
	    #endif	

	    // Is[2:NI] chain
	    #ifdef DEBUGPRINT
	    std::cout << "dIs[2:NI]/dt..." << std::flush;		
	    #endif	

	    thrust::transform(Is_begin+1,Is_begin+NI,Is_begin,dIsdt_begin+1,
	    NI*Mu*_2-NI*Mu*_1);	

	    #ifdef DEBUGPRINT
	    std::cout << "done" << std::endl;	
	    #endif	

	    // correction last Is 
	    // last Is[NI] receives from Is[NI-1] and can recover or die with prob fracMs and rate Mu	
	    /*#ifdef DEBUGPRINT
	    std::cout << "dI[NI]/dt..." << std::flush;		
	    #endif	

	    Is=*(Is_end-1); //last Is
	    value_type Is_prev=*(Is_end-2);
	    *(dIsdt_end-1)= NI*Mu*Is_prev - NI*Mu*Is;	

	    #ifdef DEBUGPRINT
	    std::cout << "done" << std::endl;	
	    #endif	
	    */
	
	    ////////////////////////////////////////////////////////
	    // dI_a/dt = (1-frac)*Alpha*E - Gamma*I_a  
	    #ifdef DEBUGPRINT
	    std::cout << "dIa[1]/dt..." << std::flush;
	    #endif	

	    value_type Ia=*Ia_begin;	    		 			
	    *dIadt_begin =  frac_a*Alpha*NE*E -Gamma*NI*Ia; 	

	    #ifdef DEBUGPRINT
	    std::cout << "listo" << std::endl;
	    #endif	

	    // cadena de ecuaciones Ia
	    #ifdef DEBUGPRINT
	    std::cout << "dIa[2:NI]/dt..." << std::flush;
	    #endif	

	    thrust::transform(Ia_begin+1,Ia_begin+NI,Ia_begin,dIadt_begin+1,
	    NI*Gamma*_2-NI*Gamma*_1);	

	    #ifdef DEBUGPRINT
	    std::cout << "listo" << std::endl;
	    #endif	

	    ////////////////////////////////////////////////////////
	    // dR/dt = Gamma*(I_a+I_s) - Mu*R 
	    #ifdef DEBUGPRINT
	    std::cout << "dR/dt..." << std::flush;
	    #endif	

	    Is=*(Is_end-1);
	    Ia=*(Ia_end-1);	
	    value_type R=*R_begin;	    		 			
	    *dRdt_begin =  Mu*NI*Is*(1.0-fracM)+Gamma*NI*Ia; 	

	    #ifdef DEBUGPRINT
	    std::cout << "listo" << std::endl;
	    #endif	


	    ////////////////////////////////////////////////////////
	    // dM/dt = Mu*I_s*fracM 
	    #ifdef DEBUGPRINT
	    std::cout << "dM/dt..." << std::flush;
	    #endif	

	    //Is=*(Is_end-1);
	    *dMdt_begin =  NI*Mu*Is*fracM; 

	    #ifdef DEBUGPRINT
	    std::cout << "listo" << std::endl;
	    #endif	
	}

    	// basic reproduction ratio (valid if trun long enough to reach stationarity)
	value_type R0()
	{
		value_type S=*(S_begin);
		value_type R=*(R_begin);

		value_type R0=-log(S/initialS)/R;
    		varout << "R0 = "<< R0 << std::endl;
		return R0;	
	}
};


struct write_sir_detailed{
	double CumulatedInfectedGroup_s;
	typedef std::vector<value_type> stdvec;
	stdvec &t_vec, &S_vec, &E_vec, &Is_vec, &Ia_vec, &R_vec, &M_vec, &AIs_vec;	
	
	sir_system &Sys;

	write_sir_detailed(sir_system &S, stdvec _t_vec, stdvec _S_vec, stdvec _E_vec, stdvec _Is_vec, 
	stdvec _Ia_vec, stdvec _R_vec, stdvec _M_vec, stdvec _AIs_vec):
	Sys(S), t_vec(_t_vec), S_vec(_S_vec), E_vec(_E_vec), Is_vec(_Is_vec), Ia_vec(_Ia_vec), R_vec(_R_vec),
	M_vec(_M_vec), AIs_vec(_AIs_vec)
	{
    		resout << 
		"tiempo Susceptibles Expuestos InfectedSyn InfectedAsy Recovered Dead CumuISyn" 
		<< std::endl;		
		CumulatedInfectedGroup_s=0.0;
	};

	~write_sir_detailed(){};

	void print()
	{
		for(int i=0;i<t_vec.size();i++)
		{				
			resout << t_vec[i];
		    	resout << '\t' << S_vec[i];
		    	resout << '\t' << E_vec[i];
		    	resout << '\t' << Is_vec[i];
		    	resout << '\t' << Ia_vec[i];
		    	resout << '\t' << R_vec[i];
			resout << '\t' << M_vec[i]; 
			resout << '\t' << AIs_vec[i]; 
			resout << endl;
		}
		std::cout << "listo!" << std::endl;
	};

	void operator()( const state_type &x , const double t)
	{
	    	double SuceptibleGroup=Sys.totalS();
	    	double InfectedGroup_s=Sys.totalIs();	
	    	double InfectedGroup_a=Sys.totalIa();	
	    	double ExposedGroup=Sys.totalE();
	    	double RecoveredGroup=Sys.totalR();
	    	double DeadGroup=Sys.totalM();

		CumulatedInfectedGroup_s+=InfectedGroup_s*Dt;

		t_vec.push_back(t);
		S_vec.push_back(SuceptibleGroup);		
		E_vec.push_back(ExposedGroup);		
		Is_vec.push_back(InfectedGroup_s);
		Ia_vec.push_back(InfectedGroup_a);		
		R_vec.push_back(RecoveredGroup);
		M_vec.push_back(DeadGroup);
		AIs_vec.push_back(CumulatedInfectedGroup_s);		
	}
};


int naiveSIR(value_type BETA_S, value_type BETA_A, value_type ALPHA, value_type GAMMA, 
value_type FRAC, value_type FRACM, value_type MU, value_type NU, int NE, int NI, value_type trun)
{    	
    Dt=0.05;	 

    // initial conditions (S[0],I[0],R[0])
    size_t Nvars=3+NE+2*NI; // 1(S)+NE(R)+NI(Ia)+NI(Is)+1(R)+1(M)	
    state_type x(Nvars,0.0); 
    value_type initialIs=0.00001;	
    value_type initialS=1.0-initialIs;	
    x[2]=initialIs; 
    x[0]=initialS; 

    sir_system sir(x,BETA_S,BETA_A,ALPHA,GAMMA,FRAC,FRACM,MU,NU,NE,NI,trun);

    std::cout << "Initialization ready" << std::endl;

    // monitoreo
    std::vector<value_type> t_vec, S_vec, E_vec, Is_vec, Ia_vec, R_vec, M_vec, AIs_vec;	
    write_sir_detailed monitoreo(sir,t_vec, S_vec, E_vec, Is_vec, Ia_vec, R_vec, M_vec, AIs_vec);	

    // integracion 
    runge_kutta4< state_type , value_type , state_type , value_type > stepper;
    integrate_const( stepper, sir , x , 0.0 , trun , Dt , monitoreo);

    monitoreo.print();	
 
    std::cout << sir.R0() << std::endl;	

    return 0;
}

int main(int argc, char **argv)
{
    assert(argc==12);
    value_type BETA_S=atof(argv[1]);
    value_type BETA_A=atof(argv[2]);
    value_type ALPHA=atof(argv[3]);
    value_type GAMMA=atof(argv[4]);
    value_type FRAC=atof(argv[5]);
    value_type FRACM=atof(argv[6]);
    value_type MU=atof(argv[7]);
    value_type NU=atof(argv[8]);	
    int NE=atof(argv[9]);
    int NI=atof(argv[10]);
    value_type trun=atof(argv[11]);

    cpu_timer Reloj;

    #if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_OMP
    std::cout << "OMP run" << std::endl;	
    Reloj.tic();	
    #elif THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CPP
    std::cout << "CPP run" << std::endl;	
    Reloj.tic();	
    #elif THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
    std::cout << "CUDA run" << std::endl;	
    gpu_timer Reloj_gpu;
    Reloj_gpu.tic();	
    #else	
    #endif	    	
 
    naiveSIR(BETA_S,BETA_A,ALPHA,GAMMA,FRAC,FRACM,MU,NU,NE,NI,trun);    	

    std::cout << "(cpu clock) ms=" << Reloj.tac() << std::endl;

    #if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
    std::cout << "(gpu clock) ms=" << Reloj_gpu.tac() << std::endl;
    #endif
		
    return 0;
}

