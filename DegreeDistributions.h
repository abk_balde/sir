/*
#include <iostream>
#include <fstream>
#include <boost/array.hpp>
#include <assert.h>
#include <cstdlib>
#include <vector>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>
#include <thrust/inner_product.h>

#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <boost/numeric/odeint/integrate/integrate_const.hpp>
#include <boost/numeric/odeint/external/thrust/thrust.hpp>

using namespace std;
using namespace boost::numeric::odeint;

//typedef boost::array< double , NN+2 > state_type;
typedef double value_type;
typedef thrust::device_vector<value_type> state_type;
typedef state_type::const_iterator const_it_state_type;
typedef state_type::iterator it_state_type;
*/

// scale free shape
struct scalefreeop{
	value_type pot;
	scalefreeop(value_type pot_):pot(pot_){};
	__host__ __device__ 
	value_type operator()(int k){
		return value_type(1./powf(k,pot));
	}
};

// Degree distribution class
struct DegreeDist
{
	thrust::device_vector<value_type> pi;
	thrust::device_vector<value_type> pikm1;
	value_type p;
	value_type meank;
	int Ndeg;	
	
	void output(){
		std::cout << "#Ndeg=" << Ndeg << std::endl;
		std::cout << "#p=" << p << std::endl;
		std::cout << "#meank=" << meank << std::endl;
		for(int k=0;k<Ndeg;k++)
		{
			std::cout << k << " " << pi[k] << " " << pikm1[k] << std::endl;
		}
	};

	void init(value_type _p, int _Ndeg)//:p(_p),Ndeg(_Ndeg)
	{
		p=_p;
		Ndeg=_Ndeg;

		pi.resize(Ndeg);
		using namespace thrust::placeholders;
		thrust::transform(
			thrust::make_counting_iterator(1),thrust::make_counting_iterator(Ndeg+1),
			pi.begin(), scalefreeop(p)
		);
		value_type normalizacion=thrust::reduce(pi.begin(),pi.end(),0.0);
		thrust::transform(pi.begin(),pi.end(),pi.begin(),_1/normalizacion);

		meank = thrust::inner_product(pi.begin(), pi.end(),thrust::make_counting_iterator(1),0.0f);

		pikm1.resize(Ndeg);
		thrust::transform(pi.begin(), pi.end(),thrust::make_counting_iterator(1),
				  pikm1.begin(),_1*(_2-1)/meank);				
	};	
}; 

/* 
//TESTING
#include "DegreeDistributions.h"

int main(){
	DegreeDist d;
	d.init(2,10);
	d.output();
}
*/
