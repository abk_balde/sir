====
Resuelve el modelo 

S => I1 -> I2 -> ... -> IM -> ... -> IN -> R

dS/dt = -beta * S * InfectedGroup - mu*S

dI_1/dt = beta * S * InfectedGroup - gamma * N * I_1 - mu * I_1

dI_i/dt = gamma * N * I_{i-1} - gamma * N * I_i - mu * I_i

dR/dt = GAMMA * I_N - mu * R

InfectedGroup =  I_{M+1} + I_2 + ... + I_N

ExposedGroup = I_1 + I_2 + ... + I_M

Ref.
Modeling Infectious Diseases IN HUMANS AND ANIMALS,
Matt J. Keeling and Pejman Rohani,
2008 by Princeton University Press

Ecuaciones del modelo:
3.3.1. SEIR and Multi-Compartment Models, pag 94, eq 3.13

====
0) Requerimientos:

Tener instalado boost odeint
http://headmyshoulder.github.io/odeint-v2/

En Ubuntu:

$ sudo apt-get install libboost-all-dev

Tener instalado gnuplot

En Ubuntu:

$ sudo apt-get install gnuplot

1) Compilacion: 
Con valores default

$ make

Con valores propios

$ make BETA=xxxx GAMMA=yyyy NN=zzzz MM=uuuu ....

2) Compilar y Correr. Ejemplos:

* hasta t=100 (default) sin salida grafica

$ make run 

* hasta t=xxxx sin salida grafica

$ make run TRUN=xxxx 

* hasta t=xxxx con salida grafica en gnuplot

$ make gnuplot TRUN=xxxx 

* hasta t=xxxx con salida grafica en gnuplot y BETA no default

$ make gnuplot TRUN=xxxx BETA=yyyy

3) Ver output crudo en "results.dat"

====

