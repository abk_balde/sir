/*
====
Solves the model

S => E -> {Is,Ia} -> R

InfectedGroup_s=Is	
InfectedGroup_a=Ia	

dS/dt = -S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Mu*S + Nu
dE/dt = S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Alpha*E - Mu*E
dI_s/dt = frac*Alpha*E - Gamma*I_s - Mu*I_s 
dI_a/dt = (1-frac)*Alpha*E - Gamma*I_a - Mu*I_a 
dR/dt = Gamma*(I_a+I_s) - Mu*R 

S ---> E ----> Ia ------> R
	 `---> Is ---'

frac is the E->Is sintomatic fractions.
(1-frac) is the E->Ia asintomatic fraction.

Alpha=incubation rate
Gamma=recovery rate
Beta_s=sintomatic transmission rate
Beta_a=asintomatic transmission rate
Mu=death rate
Nu=death rate

*/

#if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
#include "gpu_timer.h"
#endif

#include "cpu_timer.h"


#include <iostream>
#include <fstream>
#include <boost/array.hpp>
#include <assert.h>
#include <cstdlib>
#include <vector>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>

#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <boost/numeric/odeint/integrate/integrate_const.hpp>
#include <boost/numeric/odeint/external/thrust/thrust.hpp>

using namespace std;
using namespace boost::numeric::odeint;

/////////////////////////////////////////////////////////////
double Dt;

//typedef boost::array< double , NN+2 > state_type;
typedef double value_type;
typedef thrust::device_vector<value_type> state_type;
typedef state_type::const_iterator const_it_state_type;
typedef state_type::iterator it_state_type;

ofstream varout("vars.gnu");	
ofstream resout("results.dat");

class sir_system{
	private:
	const_it_state_type S_begin, S_end;		
	const_it_state_type E_begin, E_end;		
	const_it_state_type Is_begin, Is_end;		
	const_it_state_type Ia_begin, Ia_end;		
	const_it_state_type R_begin, R_end;		

	int NE,NI; // chain lengths 
	value_type Beta_s, Beta_a, Alpha, Gamma, frac, Mu, Nu;	
	value_type Trun;
	value_type initialS;
	value_type frac_s, frac_a;

	public:
	sir_system(const state_type &x,  
	value_type Beta_s_, value_type Beta_a_, value_type Alpha_, 
	value_type Gamma_, value_type frac_, value_type Mu_, 
	value_type Nu_, int NE_, int NI_, value_type TRUN_):
	Beta_s(Beta_s_),Beta_a(Beta_a_),Alpha(Alpha_),Gamma(Gamma_),frac(frac_),Mu(Mu_),Nu(Nu_),NE(NE_),NI(NI_),Trun(TRUN_)
	{
		S_begin=x.begin(); S_end=S_begin+1; //S
		E_begin=S_end; E_end=E_begin+NE; //SE
		Is_begin=E_end; Is_end=Is_begin+NI; //SEIs
		Ia_begin=Is_end; Ia_end=Ia_begin+NI; //SEIsIa
		R_begin=Ia_end; R_end=R_begin+1; //SEIsIaR	
		
		initialS=x[0];	

		frac_s=frac;
		frac_a=(1.0-frac_s);

    		varout 
		<< "beta_s=" << Beta_s << "; beta_a=" << Beta_a 
		<< "; alpha=" << Alpha << "; gamma=" << Gamma 
		<< "; fracs=" << frac_s << "; nu=" << Nu 
		<< "; mu=" << Mu << "; trun=" << Trun 
		<< "; NE=" << NE << "; NI=" << NI 
		<< std::endl;
	}

	void operator()( const state_type &x , state_type &dxdt , const value_type t )
	{
	    // x[0] = S == susceptible
	    // x[1],x[2],...,x[M] = I_1, I_2,..., IM == exposed 
	    // x[M+1],x[2],...,x[N] = I_1, I_2,..., IN == infected
	    // x[N+1]= R == recovered

	    // mapping iterators to classes
	    it_state_type dSdt_begin=dxdt.begin(); it_state_type dSdt_end=dSdt_begin+1; //S
	    it_state_type dEdt_begin=dSdt_end; it_state_type dEdt_end=dEdt_begin+NE; //SE
	    it_state_type dIsdt_begin=dEdt_end; it_state_type dIsdt_end=dIsdt_begin+NI; //SEIs
	    it_state_type dIadt_begin=dIsdt_end; it_state_type dIadt_end=dIadt_begin+NI;//SEIsIa
	    it_state_type dRdt_begin=dIadt_end; it_state_type dRdt_end=dRdt_begin+1;//SEIsIaR		

	    double InfectedGroup_s=thrust::reduce(Is_begin,Is_end);//=*Is_begin; 	
	    double InfectedGroup_a=thrust::reduce(Ia_begin,Ia_end);//=*Ia_begin; 	
	
	    // dS/dt = -S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Mu*S + Nu*S
	    value_type S=*S_begin;	    		 			
	    *dSdt_begin = -S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Mu*S + Nu; 			
	
	    ////////////////////////////////////////////////////////
	    // dE/dt = S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Alpha*E - mu*E
	    value_type E=*E_begin;	    		 			
	    *dEdt_begin =  S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - NE*Alpha*E -NE*Mu*E; 	
	
	    // cadena de ecuaciones E
	    using namespace thrust::placeholders;
	    thrust::transform(E_begin+1,E_begin+NE,E_begin,dEdt_begin+1,
	    NE*Alpha*_2-NE*Alpha*_1-NE*Mu*_1);	
	    E=*(E_end-1); //last E	    		 			

	    ////////////////////////////////////////////////////////
	    // dI_s/dt = frac*Alpha*E - Gamma*I_s - Mu*I_s 
	    value_type Is=*Is_begin;	    		 			
	    *dIsdt_begin =  frac_s*Alpha*NE*E -Gamma*NI*Is -Mu*NI*Is; 	

	    // cadena de ecuaciones Is
	    thrust::transform(Is_begin+1,Is_begin+NI,Is_begin,dIsdt_begin+1,
	    NI*Gamma*_2-NI*Gamma*_1-NI*Mu*_1);	

	    ////////////////////////////////////////////////////////
	    // dI_a/dt = (1-frac)*Alpha*E - Gamma*I_a - Mu*I_a 
	    value_type Ia=*Ia_begin;	    		 			
	    *dIadt_begin =  frac_a*Alpha*NE*E -Gamma*NI*Ia -Mu*NI*Ia; 	

	    // cadena de ecuaciones Ia
	    thrust::transform(Ia_begin+1,Ia_begin+NI,Ia_begin,dIadt_begin+1,
	    NI*Gamma*_2-NI*Gamma*_1-NI*Mu*_1);	

	    ////////////////////////////////////////////////////////
	    // dR/dt = Gamma*(I_a+I_s) - Mu*R 
	    Is=*(Is_end-1);
	    Ia=*(Ia_end-1);	
	    value_type R=*R_begin;	    		 			
	    *dRdt_begin =  Gamma*NI*(Is+Ia) - Mu*R; 	
	}

    	// basic reproduction ratio (valid if trun long enough to reach stationarity)
	value_type R0()
	{
		value_type S=*(S_begin);
		value_type R=*(R_begin);

		value_type R0=-log(S/initialS)/R;
    		varout << "R0 = "<< R0 << std::endl;
		return R0;	
	}
};


struct write_sir_detailed{
	double CumulatedInfectedGroup_s;
	int NE,NI;

	std::vector<value_type> t_vec, S_vec, E_vec, Is_vec, Ia_vec, R_vec, AIs_vec;	

	write_sir_detailed(int NE_,int NI_):NE(NE_),NI(NI_){
    		resout << "t S E Is Ia R Acum(Is)" << std::endl;		
		CumulatedInfectedGroup_s=0.0;
	};

	~write_sir_detailed(){
		for(int i=0;i<t_vec.size();i++)
		{				
			resout << t_vec[i];
		    	resout << '\t' << S_vec[i];
		    	resout << '\t' << E_vec[i];
		    	resout << '\t' << Is_vec[i];
		    	resout << '\t' << Ia_vec[i];
		    	resout << '\t' << R_vec[i];
			resout << '\t' << AIs_vec[i]; 
			resout << endl;
		}
	};


	void operator()( const state_type &x , const double t)
	{

		const_it_state_type S_begin=x.begin(); 
		const_it_state_type S_end=S_begin+1; //S

	   	const_it_state_type E_begin=S_end; 
		const_it_state_type E_end=E_begin+NE; //SE

	   	const_it_state_type Is_begin=E_end; 
		const_it_state_type Is_end=Is_begin+NI; //SEIs

	  	const_it_state_type Ia_begin=Is_end; 
		const_it_state_type Ia_end=Ia_begin+NI; //SEIsIa

		const_it_state_type R_begin=Ia_end; 
		const_it_state_type R_end=R_begin+1; //SEIsIaR	

	    	double SuceptibleGroup=thrust::reduce(S_begin,S_end);
	    	double InfectedGroup_s=thrust::reduce(Is_begin,Is_end);	
	    	double InfectedGroup_a=thrust::reduce(Ia_begin,Ia_end);	
	    	double ExposedGroup=thrust::reduce(E_begin,E_end);
	    	double RecoveredGroup=thrust::reduce(R_begin,R_end);

		CumulatedInfectedGroup_s+=InfectedGroup_s*Dt;

		t_vec.push_back(t);
		S_vec.push_back(SuceptibleGroup);		
		E_vec.push_back(ExposedGroup);		
		Is_vec.push_back(InfectedGroup_s);
		Ia_vec.push_back(InfectedGroup_a);		
		R_vec.push_back(RecoveredGroup);
		AIs_vec.push_back(CumulatedInfectedGroup_s);		

	    	/*resout << t;
	    	resout << '\t' << SuceptibleGroup;
	    	resout << '\t' << ExposedGroup;
	    	resout << '\t' << InfectedGroup_s;
	    	resout << '\t' << InfectedGroup_a;
	    	resout << '\t' << RecoveredGroup;
		resout << '\t' << CumulatedInfectedGroup_s; 
		resout << endl;*/
	}
};


int naiveSIR(value_type BETA_S, value_type BETA_A, value_type ALPHA, value_type GAMMA, 
value_type FRAC, value_type MU, value_type NU, int NE, int NI, value_type trun)
{    	
    Dt=0.1;	 

    // initial conditions (S[0],I[0],R[0])
    size_t Nvars=2+NE+2*NI; // 1(S)+NE(R)+NI(Ia)+NI(Is)+1(R)	
    state_type x(Nvars,0.0); 
    value_type initialIs=0.001;	
    value_type initialS=1.0-initialIs;	
    x[2]=initialIs; 
    x[0]=initialS; 

    sir_system sir(x,BETA_S,BETA_A,ALPHA,GAMMA,FRAC,MU,NU,NE,NI,trun);

    // integracion 
    runge_kutta4< state_type , value_type , state_type , value_type > stepper;
    //integrate_const( stepper, sir , x , 0.0 , trun , Dt , write_sir_detailed(NE,NI));
    integrate(sir , x , 0.0 , trun , Dt , write_sir_detailed(NE,NI));
    
 
    std::cout << sir.R0() << std::endl;	

    return 0;
}

int main(int argc, char **argv)
{
    assert(argc==11);
    value_type BETA_S=atof(argv[1]);
    value_type BETA_A=atof(argv[2]);
    value_type ALPHA=atof(argv[3]);
    value_type GAMMA=atof(argv[4]);
    value_type FRAC=atof(argv[5]);
    value_type MU=atof(argv[6]);
    value_type NU=atof(argv[7]);	
    int NE=atof(argv[8]);
    int NI=atof(argv[9]);
    value_type trun=atof(argv[10]);

    cpu_timer Reloj;

    #if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_OMP
    std::cout << "OMP run" << std::endl;	
    Reloj.tic();	
    #elif THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CPP
    std::cout << "CPP run" << std::endl;	
    Reloj.tic();	
    #elif THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
    std::cout << "CUDA run" << std::endl;	
    gpu_timer Reloj_gpu;
    Reloj_gpu.tic();	
    #else	
    #endif	    	
 
    naiveSIR(BETA_S,BETA_A,ALPHA,GAMMA,FRAC,MU,NU,NE,NI,trun);    	

    std::cout << "(cpu clock) ms=" << Reloj.tac() << std::endl;

    #if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
    std::cout << "(gpu clock) ms=" << Reloj_gpu.tac() << std::endl;
    #endif
		
    return 0;
}

