/*
====
Solves the model

S => E -> {Is,Ia} -> R

with Ndef degrees

S ---> E ----> Ia ------> R
	 `---> Is ----'
		`-------> M

frac is the E->Is sintomatic fractions.
(1-frac) is the E->Ia asintomatic fraction.

Alpha=incubation rate
Gamma=recovery rate
Beta_s=sintomatic transmission rate
Beta_a=asintomatic transmission rate
Mu=death rate
Nu=death rate

*/

#include <iostream>
#include <fstream>
#include <boost/array.hpp>
#include <assert.h>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>
#include <thrust/inner_product.h>
#include <thrust/fill.h>

#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <boost/numeric/odeint/integrate/integrate_const.hpp>
#include <boost/numeric/odeint/external/thrust/thrust.hpp>

using namespace std;
using namespace boost::numeric::odeint;

typedef double value_type;
typedef thrust::device_vector<value_type> state_type;
typedef state_type::const_iterator const_it_state_type;
typedef state_type::iterator it_state_type;

#if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
#include "gpu_timer.h"
#endif

#include "cpu_timer.h"

#include "DegreeDistributions.h"

/////////////////////////////////////////////////////////////
// global variables...
ofstream varout("vars.gnu");	
ofstream resout("results.dat");

#ifndef Dt
double Dt=0.1;
#endif

struct Parametros{
	// Alpha, Beta_a, Beta_b, Gamma, Mu, fracs, frac_m, Ndeg, ps, pa, 
	value_type Alpha;
	value_type Beta_a;
	value_type Beta_s;
	value_type Gamma;
	value_type Mu;
	value_type frac_s;
	value_type frac_m;
	int Ndeg;
	value_type p_Ia;
	value_type p_Is;
	value_type p_S;

	void output(){
    		varout 
		<< "; Alpha="  << Alpha  << "; Beta_a=" << Beta_a 
		<< "; Beta_s=" << Beta_s << "; Gamma="  << Gamma 
		<< "; Mu="     << Mu   
		<< "; frac_s=" << frac_s << "; frac_m="  << frac_m  
		<< "; Ndeg="   << Ndeg  
		<< "; p_Is="   << p_Is    << "; p_Ia="    << p_Ia 
		<< "; p_S="    << p_S     
		<< ";" 	       << std::endl;
	}
};

class sir_system{
	private:

	const_it_state_type S_begin, S_end;   // Susceptible iterators		
	const_it_state_type E_begin, E_end;   // Exposed iterators		
	const_it_state_type Is_begin, Is_end; // Infected Syntomatic iterators		
	const_it_state_type Ia_begin, Ia_end; // Infected Asyntomatic iterators		
	const_it_state_type R_begin, R_end;   // Recovered iterators		
	const_it_state_type M_begin, M_end;   // Dead iterators		

	DegreeDist Dist_Ia;    			// pi_a[k] degree distribution asymtomatic
	DegreeDist Dist_Is; 			// pi_s[k] degree distribution symptomatic
	DegreeDist Dist_S; 			// pi_s[k] degree distribution symptomatic
	thrust::device_vector<value_type> kS; 	// k S[k]

	int Ndeg; 					// number of degrees 
	value_type p_Is, p_Ia, p_S; 			// degree distribution powers
	value_type Alpha, Beta_a, Beta_s, Gamma, Mu; 	// rates	
	value_type frac_s, frac_m;			// fractions  
	value_type initialS; 	 			// initial Susceptibles
	value_type Trun; 				// total time 

	public:

	value_type totalS()
	{
		return thrust::inner_product(S_begin,S_end,Dist_S.pi.begin(),0.0); 	
	}
	value_type totalE()
	{
		return thrust::inner_product(E_begin,E_end,Dist_S.pi.begin(),0.0); 	
	}
	value_type totalIs()
	{
		return thrust::inner_product(Is_begin,Is_end,Dist_Is.pi.begin(),0.0); 	
	}
	value_type totalIa()
	{
		return thrust::inner_product(Ia_begin,Ia_end,Dist_Ia.pi.begin(),0.0); 	
	}
	value_type totalR()
	{
		return thrust::inner_product(R_begin,R_end,Dist_S.pi.begin(),0.0); 	
	}
	value_type totalM()
	{
		return thrust::reduce(M_begin,M_end); 	
	}

	sir_system(const state_type &x, Parametros params, value_type initialI)
	{		
		// 10 PARAMETERS
		// Alpha, Beta_a, Beta_b, Gamma, Mu, fracs, frac_m, Ndeg, ps, pa, 

		Alpha = params.Alpha;
		Beta_a = params.Alpha;
		Beta_s = params.Alpha;
		Gamma = params.Gamma;
		Mu = params.Mu;
		frac_m = params.frac_m;
		frac_s = params.frac_s;
		Ndeg = params.Ndeg;
		p_Ia = params.p_Ia;
		p_Is = params.p_Is;
		
		initialS=1.0-initialI;
		/*std::cin >> Ndeg;
		std::cin >> p_s >> p_a;
		std::cin >> Alpha >> Beta_a >> Beta_b >> Gamma >> Mu;
		std::cin >> frac_s >> frac_m;
		std::cin >> Trun;
		*/

		// iterators to classes
		S_begin=x.begin(); S_end=S_begin+Ndeg; 	//S
		E_begin=S_end; E_end=E_begin+Ndeg; 	//SE
		Is_begin=E_end; Is_end=Is_begin+Ndeg; 	//SEIs
		Ia_begin=Is_end; Ia_end=Ia_begin+Ndeg; 	//SEIsIa
		R_begin=Ia_end; R_end=R_begin+Ndeg; 	//SEIsIaR	
		M_begin=R_end; M_end=M_begin+1; 	//SEIsIaRM (1 degree)	

		Dist_Ia.init(p_Ia,Ndeg); 
 		Dist_Is.init(p_Is,Ndeg);
 		Dist_S.init(p_S,Ndeg);
		kS.resize(Ndeg);

		// initial conditions
		thrust::fill(S_begin,S_end,1.0-initialI);
		thrust::fill(Ia_begin,Ia_end,initialI);
		thrust::fill(Is_begin,Is_end,initialI);
		thrust::fill(R_begin,R_end,0.0);
		thrust::fill(M_begin,M_end,0.0);

		params.output();
	}

	void operator()( const state_type &x , state_type &dxdt , const value_type t )
	{
	    // iterators to derivatives d/dt ...
	    it_state_type dSdt_begin=dxdt.begin(); it_state_type dSdt_end=dSdt_begin+Ndeg; 	//S
	    it_state_type dEdt_begin=dSdt_end; it_state_type dEdt_end=dEdt_begin+Ndeg; 		//SE
	    it_state_type dIsdt_begin=dEdt_end; it_state_type dIsdt_end=dIsdt_begin+Ndeg; 	//SEIs
	    it_state_type dIadt_begin=dIsdt_end; it_state_type dIadt_end=dIadt_begin+Ndeg;	//SEIsIa
	    it_state_type dRdt_begin=dIadt_end; it_state_type dRdt_end=dRdt_begin+Ndeg;		//SEIsIaR		
	    it_state_type dMdt_begin=dRdt_end; it_state_type dMdt_end=dMdt_begin+1;		//SEIsIaRM		

	    // degrees iterator
	    auto degrees_begin=thrust::make_counting_iterator(1);
	    auto degrees_end=thrust::make_counting_iterator(Ndeg+1);

	    ////////////////////////////////////////////////////////	    
	    // dS[k]/dt = -S[k]*(k/<k>)* Sum_{k'} {Beta_s*pi_Is(k')(k'-1) Is[k] + Beta_a*pi_Ia(k')(k'-1)};
	    // == -S[k]*k*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s), k=1,Ndeg
	    // == -S[k]*k*InfectedGroup

	    double InfectedGroup_s = thrust::inner_product(Is_begin,Is_end,Dist_Is.pikm1.begin(),0.0); 	
	    double InfectedGroup_a = thrust::inner_product(Ia_begin,Ia_end,Dist_Ia.pikm1.begin(),0.0); 		
	    double InfectedGroup = Beta_a*InfectedGroup_a+Beta_s*InfectedGroup_s;

	    #ifdef DEBUGPRINT
	    std::cout << "dS[k]/dt..." << std::flush;		
	    #endif	

	    // k*S[k]
	    using namespace thrust::placeholders;
	    thrust::transform(S_begin,S_end,degrees_begin,kS.begin(),_1*_2);

	    // dS[k]/dt
	    thrust::transform(kS.begin(),kS.end(),dSdt_begin,-InfectedGroup*_1);

	    #ifdef DEBUGPRINT
	    std::cout << "done" << std::endl;	
	    #endif	
	
	    ////////////////////////////////////////////////////////
	    // dE[k]/dt = Beta*S[k]*k* Sum_{k'} {pi_s(k')(k'-1) Is[k] + pi_a(k')(k'-1) Ia[k]}- Alpha*E[k];
            // 		= S[k]*k*InfectedGroup - Alpha*E[k];
	    #ifdef DEBUGPRINT
	    std::cout << "dE[k]/dt..." << std::flush;		
	    #endif	

	    // dE[k]/dt
	    thrust::transform(kS.begin(),kS.end(),E_begin,dEdt_begin,InfectedGroup*_1-Alpha*_2);

	    #ifdef DEBUGPRINT
	    std::cout << "done" << std::endl;	
	    #endif	
	
	    ////////////////////////////////////////////////////////
	    // dI_s[k]/dt = frac_s*Alpha*E[k] - Mu*I_s[k] 

	    #ifdef DEBUGPRINT
	    std::cout << "dIs[k]/dt..."<< std::flush;	
	    #endif	

	    // dI_s[k]/dt	
	    thrust::transform(E_begin,E_end,Is_begin,dIsdt_begin,frac_s*Alpha*_1-Mu*_2);

	    #ifdef DEBUGPRINT
	    std::cout << "done" << std::endl;	
	    #endif	
	
	    ////////////////////////////////////////////////////////
	    // dI_a[k]/dt = (1-frac_s)*Alpha*E[k] - Gamma*I_a[k]  
	    #ifdef DEBUGPRINT
	    std::cout << "dIa[k]/dt..." << std::flush;
	    #endif	

	    thrust::transform(E_begin,E_end,Ia_begin,dIadt_begin,(1.0-frac_s)*Alpha*_1-Gamma*_2);

	    #ifdef DEBUGPRINT
	    std::cout << "listo" << std::endl;
	    #endif	

	    ////////////////////////////////////////////////////////
	    // dR[k]/dt = Mu*(1-frac_m)*I_a[k] + Gamma*I_s[k] 
	    #ifdef DEBUGPRINT
	    std::cout << "dR[k]/dt..." << std::flush;
	    #endif	

	    // dR[k]/dt	
	    thrust::transform(Is_begin,Is_end,Ia_begin,dRdt_begin,_1*Mu*(1.0-frac_m)+_2*Gamma);

	    #ifdef DEBUGPRINT
	    std::cout << "listo" << std::endl;
	    #endif	

	    ////////////////////////////////////////////////////////
	    // dM/dt = Mu*I_s*frac_m 
	    #ifdef DEBUGPRINT
	    std::cout << "dM/dt..." << std::flush;
	    #endif	

	    value_type allIs = thrust::reduce(Is_begin,Is_end);

	    *dMdt_begin = Mu*frac_m*allIs;	

	    #ifdef DEBUGPRINT
	    std::cout << "listo" << std::endl;
	    #endif	
	}

    	// basic reproduction ratio (valid if trun long enough to reach stationarity)
	value_type R0()
	{
		value_type S=totalS();
		value_type R=totalR();

		value_type R0=-log(S/initialS)/R;
    		varout << "R0 = "<< R0 << std::endl;
		return R0;	
	}
};


struct write_sir_detailed{
	sir_system &Sys;

	typedef std::vector<value_type> stdvec;
	stdvec &t_vec, &S_vec, &E_vec, &Is_vec, &Ia_vec, &R_vec, &M_vec;	

	write_sir_detailed(sir_system &S, 
	stdvec &_tvec, stdvec &_Svec, stdvec &_Evec, stdvec &_Isvec, stdvec &_Iavec, stdvec &_Rvec, stdvec &_Mvec)
	:Sys(S),t_vec(_tvec),S_vec(_Svec),E_vec(_Evec),Is_vec(_Isvec),Ia_vec(_Iavec),R_vec(_Rvec),M_vec(_Mvec)
	{
    		resout 
		<< "tiempo Susceptibles Expuestos InfectedSyn InfectedAsy Recovered Dead" 
		<< std::endl;
	};

	~write_sir_detailed(){}

	void print()
	{
		std::cout << "printing results " << t_vec.size() << std::endl;	
		for(int i=0;i< t_vec.size();i++)
		{				
			resout << t_vec[i];
		    	resout << '\t' << S_vec[i];
		    	resout << '\t' << E_vec[i];
		    	resout << '\t' << Is_vec[i];
		    	resout << '\t' << Ia_vec[i];
		    	resout << '\t' << R_vec[i];
			resout << '\t' << M_vec[i]; 
			resout << endl;
		}
		
		auto it=std::max_element(Is_vec.begin(),Is_vec.end());
		value_type maximo = *it;
		resout << "#maxIs= " << maximo ;
		int nmax= (it-Is_vec.begin());
		resout << " #t= " << t_vec[nmax] << std::endl;

		it=std::max_element(Ia_vec.begin(),Ia_vec.end());
		maximo = *it;
		resout << "#max Ia=" << maximo ;
		nmax= (it-Ia_vec.begin());
		resout << " #t= " << t_vec[nmax] << std::endl;
		resout << "#R0= " << Sys.R0() << std::endl;	
	};


	void operator()( const state_type &x , const double t)
	{
	    	double SuceptibleGroup=Sys.totalS();
	    	double InfectedGroup_s=Sys.totalIs();	
	    	double InfectedGroup_a=Sys.totalIa();	
	    	double ExposedGroup=Sys.totalE();
	    	double RecoveredGroup=Sys.totalR();
	    	double DeadGroup=Sys.totalM();

		t_vec.push_back(t);
		S_vec.push_back(SuceptibleGroup);		
		E_vec.push_back(ExposedGroup);		
		Is_vec.push_back(InfectedGroup_s);
		Ia_vec.push_back(InfectedGroup_a);		
		R_vec.push_back(RecoveredGroup);
		M_vec.push_back(DeadGroup);
	}
};


int naiveSIR(int argc, char **argv)
{    	
    // 10 PARAMETERS
    // Alpha, Beta_a, Beta_s, Gamma, Mu, fracs, frac_m, Ndeg, pa, ps, 

    Parametros params;		

    assert(argc==13);

    params.Alpha=atof(argv[1]);
    params.Beta_a=atof(argv[2]);
    params.Beta_s=atof(argv[3]);
    params.Gamma=atof(argv[4]);
    params.Mu=atof(argv[5]);
    params.frac_s=atof(argv[6]);
    params.frac_m=atof(argv[7]);
    params.Ndeg=atoi(argv[8]);
    params.p_Ia=atof(argv[9]);
    params.p_Is=atof(argv[10]);
    params.p_S=atof(argv[11]);

    double trun=atof(argv[12]);	 
    int Ndeg=params.Ndeg;
	
    // initial conditions (S[0],I[0],R[0])
    size_t Nvars=(params.Ndeg)*5+1; // Ndeg x (S+E+Ia+Is+R) + 1 x M	
    state_type x(Nvars,0.0); 
    value_type initialI=0.00001;	


    std::cout << "Initialization...";
    sir_system sir(x, params, initialI);
    std::cout << "ready" << std::endl;


    // monitoreo
    std::vector<value_type> t_vec, S_vec, E_vec, Is_vec, Ia_vec, R_vec, M_vec;	
    write_sir_detailed monitoreo(sir,t_vec, S_vec, E_vec, Is_vec, Ia_vec, R_vec, M_vec);	

    // integracion 
    runge_kutta4< state_type , value_type , state_type , value_type > stepper;
    integrate_const( stepper, sir , x , 0.0 , trun , Dt , monitoreo);

    monitoreo.print();	

    //std::cout << sir.R0() << std::endl;	

    return 0;
}

int main(int argc, char **argv)
{
    cpu_timer Reloj;

    #if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_OMP
    std::cout << "OMP run" << std::endl;	
    Reloj.tic();	
    #elif THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CPP
    std::cout << "CPP run" << std::endl;	
    Reloj.tic();	
    #elif THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
    std::cout << "CUDA run" << std::endl;	
    gpu_timer Reloj_gpu;
    Reloj_gpu.tic();	
    #else	
    #endif	    	
 
    naiveSIR(argc,argv);    	

    std::cout << "(cpu clock) ms=" << Reloj.tac() << std::endl;

    #if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
    std::cout << "(gpu clock) ms=" << Reloj_gpu.tac() << std::endl;
    #endif
		
    return 0;
}

