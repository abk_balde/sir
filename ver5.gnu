if(first==0){
res
load 'params.gnu'
load 'ascii.gnu'

first=1
system("make sir5cpp")
set term qt 0 size 700,400 title "SE{Is,Ia}RM Model"
}

comando=sprintf("make run5 BETA_S=%f BETA_A=%f ALPHA=%f GAMMA=%f FRAC=%f FRACM=%f MU=%f NU=%f NE=%d NI=%d TRUN=%d",\
BETA_S,BETA_A,ALPHA,GAMMA,FRAC,FRACM,MU,NU,NE,NI,TRUN);

print comando


system(comando)
load 'vars.gnu'

#pause .5

titulo=sprintf("{/Symbol b}_s=%1.3f {/Symbol b}_a=%1.3f {/Symbol a}=%1.3f {/Symbol g}=%1.3f FRAC_s=%1.3f FRACM=%1.3f {/Symbol m}=%1.3f {/Symbol n}=%1.3f NE=%d NI=%d TRUN=%d R_0=%1.3f",\
BETA_S,BETA_A,ALPHA,GAMMA,FRAC,FRACM,MU,NU,NE, NI, TRUN,R0);

set log y
set tit titulo enhanced
set key autotitle columnhead bot; 

pais='china.dat'

plot [][1e-5:1] for[i=2:7] 'results.dat' u 1:(column(i)) w l lw (i==7 || i==4 || i==5)?(3):(1) lc i,\
1e-06*2**(x/TDUPLICACION) t sprintf('2^{t(dias)/%f}',TDUPLICACION) lt 0 lw 3 lc 0,\
pais u ($0-SHIFTREAL):($1/SCALEREAL) t columnhead,\
pais u ($0-SHIFTREAL):($2/SCALEREAL) t columnhead


#'cumulated_deaths_global.dat' u ($0-SHIFTREAL):($2/SCALEREAL) t columnhead,\
#'cumulated_global.dat' u ($0-SHIFTREAL):($2/SCALEREAL) t columnhead

pause mouse key "Sobre el plot apretar las teclas:\n(A,a)->alpha\n(B,b)->Betas\n(V,v)->Betaa\n(F,f)->fracs\n(D,d)->mu\n(G,g)->Gamma\n(M, m)->fracM\n(K, k)->#E\n(L, l)->#I\n(T,t)->trun\n(Y,y)->tduplicacion\n(U,u)->shift datos\n(I,i)->escaleo datos\npara (subir,bajar) valores\nO espacio para terminar...\n"

inc=0.02
inc2=inc*0.1


#65 A
#97 a
if(MOUSE_KEY==65){ALPHA=ALPHA+inc;}
if(MOUSE_KEY==97){ALPHA=(ALPHA>inc)?(ALPHA-inc):(0.0);}

#66 B
#98 b
if(MOUSE_KEY==66){BETA_S=BETA_S+inc; }
if(MOUSE_KEY==98){BETA_S=(BETA_S>=inc)?(BETA_S-inc):(0.0);}

#68 D
#100 d
if(MOUSE_KEY==68){MU=MU+inc; }
if(MOUSE_KEY==100){MU=(MU>=inc)?(MU-inc):(0.0);}


#70 F
#102 f
if(MOUSE_KEY==70) {FRAC=(FRAC<1.0-inc2)?(FRAC+inc2):(1.0); }
if(MOUSE_KEY==102) {FRAC=(FRAC>=inc2)?(FRAC-inc2):(0.0); }

#71 G
#103 g
if(MOUSE_KEY==71) {GAMMA=GAMMA+inc; }
if(MOUSE_KEY==103) {GAMMA=(GAMMA>=inc)?(GAMMA-inc):(0.0);}

#84 T
#116 t
if(MOUSE_KEY==84) {TRUN=TRUN+50; }
if(MOUSE_KEY==116) {TRUN=(TRUN>50)?(TRUN-50):(0); }

#75 K
#107 k
if(MOUSE_KEY==75) {NE=NE+1; }
if(MOUSE_KEY==107) {NE=(NE>1)?(NE-1):(1); }

#76 L
#108 l
if(MOUSE_KEY==76) {NI=NI+1; }
if(MOUSE_KEY==108) {NI=(NI>1)?(NI-1):(1); }

#77 M
#109 m
if(MOUSE_KEY==77) {FRACM=(FRACM<1.0-inc2)?(FRACM+inc2):(1.0); }
if(MOUSE_KEY==109) {FRACM=(FRACM>=inc2)?(FRACM-inc2):(0.0); }

#86 V
#118 v
if(MOUSE_KEY==86) {BETA_A=BETA_A+inc; }
if(MOUSE_KEY==118) {BETA_A=(BETA_A>=inc)?(BETA_A-inc):(0.0); }

if(MOUSE_KEY==ord('Y')){TDUPLICACION=TDUPLICACION+0.1;}
if(MOUSE_KEY==ord('y')){TDUPLICACION=TDUPLICACION-0.1;}

if(MOUSE_KEY==ord('U')){SHIFTREAL=SHIFTREAL+5;}
if(MOUSE_KEY==ord('u')){SHIFTREAL=SHIFTREAL-5;}

if(MOUSE_KEY==ord('I')){SCALEREAL=SCALEREAL*1.5;}
if(MOUSE_KEY==ord('i')){SCALEREAL=SCALEREAL/1.5;}

#if(MOUSE_KEY==ord('!')){d[1]=d[1]-1;}
#if(MOUSE_KEY==ord('@')){d[2]=d[2]-1;}
#if(MOUSE_KEY==ord('#')){d[3]=d[3]-1;}
#if(MOUSE_KEY==ord('$')){d[4]=d[4]-1;}
#if(MOUSE_KEY==ord('%')){d[5]=d[5]-1;}
#if(MOUSE_KEY==ord('^')){d[6]=d[6]-1;}
#if(MOUSE_KEY==ord('&')){d[7]=d[7]-1;}
#if(MOUSE_KEY==ord('*')){d[8]=d[8]-1;}
#if(MOUSE_KEY==ord('(')){d[9]=d[9]-1;}


if(MOUSE_KEY!=32) reread

