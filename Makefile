APPS=sir1

THRUSTPATH=/usr/local/cuda/include

# valores default
N ?= 2
M ?= 1
BETA ?= 1.0
GAMMA ?= 0.2
MU ?= 0.5
NU ?= 0.0
TRUN ?= 100.0
NAGES ?= 1

ALPHA ?= 1.0
BETA_A ?= 1.0
BETA_S ?= 1.0
FRAC ?= 0.1
FRACM ?= 0.2
NE ?= 1
NI ?= 1
Dt ?= 0.1
NDEG ?= 10
PIA ?= 1.0
PIS ?= 1.0
PS ?= 1.0

#$(ALPHA) $(BETA_A) $(BETA_S) $(GAMMA) $(MU) $(FRAC) $(FRACM) $(NDEG) $(PA) $(PS) $(TRUN) $(Dt)

# para cambiar los valores default a xxxx, yyyy
# make BETA=xxxx NN=yyyy etc...

# para correr un TRUN=100.0 (default)
# make run 

# para correr un tiempo dado xxxx
# make run TRUN=xxxx

all: ${APPS}

%: %.cpp
	g++ -O2 -DNN=$N -DMM=$(M) -DBETA=$(BETA) -DGAMMA=$(GAMMA) -DMU=$(MU) -DNU=$(NU) -DNAGES=$(NAGES) -o $@ $<

clean:
	rm -f ${APPS} vars.gnu results.dat;  

gnuplot:	run
		gnuplot versoluciongral.gnu

run:	all	
	./sir1 $(TRUN)


######################
# nuevo programa
sir2: sir2.cpp
	g++ -O2 -DNN=$N -DMM=$(M) -DBETA=$(BETA) -DGAMMA=$(GAMMA) -DMU=$(MU) -DNU=$(NU) -DNAGES=4 -o sir2 sir2.cpp

gnuplot2:
	clean2; run2; gnuplot versoluciongral2.gnu

clean2:
	rm -f sir2 vars.gnu results.dat;  

run2:	sir2	
	./sir2 $(TRUN)
######################


######################
# nuevo programa
sir3cuda: sir3.cu
	nvcc -O2 -o sir3 sir3.cu -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_CUDA

sir3omp: sir3.cu
	#nvcc -O2 -o sir3 sir3.cu -Xcompiler -fopenmp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP -lgomp
	cp sir3.cu sir3.cpp; 
	g++ -O2 -o sir3 sir3.cpp -fopenmp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP -lgomp -I$(THRUSTPATH)

sir3cpp: sir3.cu
	#nvcc -O2 -o sir3 sir3.cu -Xcompiler -fopenmp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP -lgomp
	cp sir3.cu sir3.cpp; 
	g++ -O2 -o sir3 sir3.cpp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_CPP -I$(THRUSTPATH)

clean3:
	rm -f sir3 vars.gnu results.dat;  

run3:		
	./sir3 $(N) $(M) $(BETA) $(GAMMA) $(MU) $(NU) $(TRUN)

gnuplot3: run3	
	gnuplot versoluciongral.gnu

######################





######################
# nuevo programa
sir4cuda: sir4.cu
	nvcc -O2 -o sir4 sir4.cu -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_CUDA

sir4omp: sir4.cu
	cp sir4.cu sir4.cpp; 
	g++ -O2 -o sir4 sir4.cpp -fopenmp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP -lgomp \
	-I$(THRUSTPATH) #-I/home/ale/Downloads/thrust/ -I/usr/local/cuda/include

sir4cpp: sir4.cu
	cp sir4.cu sir4.cpp; 
	g++ -O2 -o sir4 sir4.cpp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_CPP \
	-I$(THRUSTPATH) #-I/home/ale/Downloads/thrust/ -I/usr/local/cuda/include

clean4:
	rm -f sir4 vars.gnu results.dat;  

run4:		
	./sir4 $(BETA_S) $(BETA_A) $(ALPHA) $(GAMMA) $(FRAC) $(MU) $(NU) $(NE) $(NI) $(TRUN)

gnuplot4: run4	
	gnuplot -e "first=0" ver4.gnu

######################



######################
# nuevo programa
sir5cuda: sir5.cu
	nvcc -O2 -o sir5 sir5.cu -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_CUDA

sir5omp: sir5.cu
	cp sir5.cu sir5.cpp; 
	g++ -std=c++11  -O2 -o sir5 sir5.cpp -fopenmp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP -lgomp \
	-I$(THRUSTPATH) #-I/home/ale/Downloads/thrust/ -I/usr/local/cuda/include

sir5cpp: sir5.cu
	cp sir5.cu sir5.cpp; 
	g++ -std=c++11  -O2 -o sir5 sir5.cpp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_CPP -I$(THRUSTPATH) 
clean5:
	rm -f sir5 vars.gnu results.dat;  

run5:		
	./sir5 $(BETA_S) $(BETA_A) $(ALPHA) $(GAMMA) $(FRAC) $(FRACM) $(MU) $(NU) $(NE) $(NI) $(TRUN)

gnuplot5: run5	
	gnuplot -e "first=0" ver5.gnu


######################



######################
# nuevo programa
sir6cpp: sir6.cu
	cp sir6.cu sir6.cpp; 
	g++ -std=c++11 -O2 -o sir6 sir6.cpp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_CPP -I$(THRUSTPATH) \
	-DDt=$(Dt) #-DDEBUGPRINT
clean6:
	rm -f sir6 vars.gnu results.dat;  

run6:		
	./sir6  $(ALPHA) $(BETA_A) $(BETA_S) $(GAMMA) $(MU) $(FRAC) $(FRACM) $(NDEG) $(PIA) $(PIS) $(PS) $(TRUN)

gnuplot6: run6	
	gnuplot -e "first=0" ver6.gnu


######################

