# make a string that contains all ASCII chars from 1 to 255
ALLCHARS = ''; do for [i=1:255] {ALLCHARS = ALLCHARS.sprintf('%c',i)}
# return position of character in ALLCHARS if ch contains 1 char, -1 otherwise
ord(ch) = (strlen(ch) == 1) ? strstrt(ALLCHARS,ch): -1

# test with ASCII char 12
pr n=64,"\n" ,testch = sprintf('%c',n)," ",ord(testch)
