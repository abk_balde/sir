unset multi
res
set xla 't'; 
set yla 'State'; 

load 'vars.gnu'

title=sprintf('S');
do for[i=1:M]{title=title."E";}
do for[i=M+1:N] {title=title."I";}
title=title."R"
title=title.sprintf(", {/Symbol b}=%1.2f",beta);
title=title.sprintf(", {/Symbol g}=%1.2f",gamma);
title=title.sprintf(", {/Symbol n}=%1.2f",nu);
title=title.sprintf(", {/Symbol m}=%1.2f",mu);
title=title.sprintf(", N=%d, M=%d, NAGES=%d",N,M,NAGES);
title=title.sprintf(", R0 = %f",R0);

#set tit title


set multi lay 2,2; 

do for[a=0:3]{
	plot [0:][0:1] for[i=1:4] "results.dat" u 1:(column(1+i+4*a)) w lp title columnheader(1+i+4*a);
}; 

pause -1 "Pulse Enter aqui para terminar"

