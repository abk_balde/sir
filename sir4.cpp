/*
====
Solves the model

S => I1 -> I2 -> ... -> IM -> ... -> IN -> R

dS/dt = -beta*S*InfectedGroup - mu*S
dI_1/dt = beta*S*InfectedGroup - gamma*N*I_1 - mu*I_1
dI_i/dt = gamma*N*I_{i-1} - gamma*N*I_i - mu*I_i
dR/dt = GAMMA*I_N - mu*R

InfectedGroup =  I_{M+1} + I_2 + ... + I_N
ExposedGroup = I_1 + I_2 + ... + I_M

Ref.
Modeling Infectious Diseases IN HUMANS AND ANIMALS
Matt J. Keeling and Pejman Rohani
2008 by Princeton University Press

Ecuaciones del modelo:
3.3.1. SEIR and Multi-Compartment Models, pag 94, eq 3.13

TODO: 
* Desagregar en grados de una red scale-free.
* Desagregar en edades.
*/

#if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
#include "gpu_timer.h"
#endif

#include "cpu_timer.h"


#include <iostream>
#include <fstream>
#include <boost/array.hpp>
#include <assert.h>
#include <cstdlib>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>

#include <boost/numeric/odeint.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <boost/numeric/odeint/integrate/integrate_const.hpp>
#include <boost/numeric/odeint/external/thrust/thrust.hpp>

using namespace std;
using namespace boost::numeric::odeint;

/////////////////////////////////////////////////////////////
double Dt;

//typedef boost::array< double , NN+2 > state_type;
typedef double value_type;
typedef thrust::device_vector<value_type> state_type;
typedef state_type::const_iterator const_it_state_type;
typedef state_type::iterator it_state_type;

ofstream varout("vars.gnu");	
ofstream resout("results.dat");

class sir_system{
	private:
	const_it_state_type S_begin, S_end;		
	const_it_state_type E_begin, E_end;		
	const_it_state_type Is_begin, Is_end;		
	const_it_state_type Ia_begin, Ia_end;		
	const_it_state_type R_begin, R_end;		

	int Nn,Mm; 
	value_type Beta_s, Beta_a, Alpha, Gamma, frac, Mu, Nu;	
	value_type Trun;
	value_type initialS;
	value_type frac_s, frac_a;

	public:
	sir_system(const state_type &x,  
	value_type Beta_s_, value_type Beta_a_, value_type Alpha_, 
	value_type Gamma_, value_type frac_, value_type Mu_, 
	value_type Nu_, value_type TRUN_):
	Beta_s(Beta_s_),Beta_a(Beta_a_),Alpha(Alpha_),Gamma(Gamma_),frac(frac_),Mu(Mu_),Nu(Nu_),Trun(TRUN_)
	{
		S_begin=x.begin(); S_end=S_begin+1; //S
		E_begin=S_end; E_end=E_begin+1; //SE
		Is_begin=E_end; Is_end=Is_begin+1; //SEIs
		Ia_begin=Is_end; Ia_end=Ia_begin+1; //SEIsIa
		R_begin=Ia_end; R_end=R_begin+1; //SEIsIaR	
		
		initialS=x[0];	

		frac_s=frac;
		frac_a=(1.0-frac_s);

    		varout 
		<< "beta_s=" << Beta_s << "; beta_a=" << Beta_a 
		<< "; alpha=" << Alpha << "; gamma=" << Gamma 
		<< "; fracs=" << frac_s << "; nu=" << Nu 
		<< "; mu=" << Mu << "; trun=" << Trun << std::endl;
	}

	void operator()( const state_type &x , state_type &dxdt , const value_type t )
	{
	    // x[0] = S == susceptible
	    // x[1],x[2],...,x[M] = I_1, I_2,..., IM == exposed 
	    // x[M+1],x[2],...,x[N] = I_1, I_2,..., IN == infected
	    // x[N+1]= R == recovered

	    // mapping iterators to classes
	    it_state_type dSdt_begin=dxdt.begin(); it_state_type dSdt_end=dSdt_begin+1; //S

	    it_state_type dEdt_begin=dSdt_end; it_state_type dEdt_end=dEdt_begin+1; //SE

	    it_state_type dIsdt_begin=dEdt_end; it_state_type dIsdt_end=dIsdt_begin+1; //SEIs

	    it_state_type dIadt_begin=dIsdt_end; it_state_type dIadt_end=dIadt_begin+1;//SEIsIa

	    it_state_type dRdt_begin=dIadt_end; it_state_type dRdt_end=dRdt_begin+1;//SEIsIaR		

	    double InfectedGroup_s=*Is_begin; //thrust::reduce(Is_begin,Is_end);	
	    double InfectedGroup_a=*Ia_begin; //thrust::reduce(Ia_begin,Ia_end);	
	
	    // dS/dt = -S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Mu*S + Nu*S
	    value_type S=*S_begin;	    		 			
	    *dSdt_begin = -S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Mu*S + Nu; 			
	
	    // dE/dt = S*(beta_a*InfectedGroup_a + beta_b*InfectedGroup_b) - Alpha*E - mu*E
	    value_type E=*E_begin;	    		 			
	    *dEdt_begin =  S*(Beta_a*InfectedGroup_a + Beta_s*InfectedGroup_s) - Alpha*E -Mu*E; 	
	
	    // dI_s/dt = frac*Alpha*E - Gamma*I_s - Mu*I_s 
	    value_type Is=*Is_begin;	    		 			
	    *dIsdt_begin =  frac_s*Alpha*E -Gamma*Is -Mu*Is; 	

	    // dI_a/dt = (1-frac)*Alpha*E - Gamma*I_a - Mu*I_a 
	    value_type Ia=*Ia_begin;	    		 			
	    *dIadt_begin =  frac_a*Alpha*E -Gamma*Ia -Mu*Ia; 	

	    // dR/dt = Gamma*(I_a+I_b) - Mu*R 
	    value_type R=*R_begin;	    		 			
	    *dRdt_begin =  Gamma*(Is+Ia) - Mu*R; 	
	}

    	// basic reproduction ratio (valid if trun long enough to reach stationarity)
	value_type R0()
	{
		value_type S=*(S_begin);
		value_type R=*(R_begin);

		value_type R0=-log(S/initialS)/R;
    		varout << "R0 = "<< R0 << std::endl;
		return R0;	
	}
};


struct write_sir_detailed{
	double CumulatedInfectedGroup_s;

	write_sir_detailed(){
    		resout << "t S E Is Ia R SumIs" << std::endl;		
		CumulatedInfectedGroup_s=0.0;
	};

	void operator()( const state_type &x , const double t)
	{

		const_it_state_type S_begin=x.begin(); 
		const_it_state_type S_end=S_begin+1; //S

	   	const_it_state_type E_begin=S_end; 
		const_it_state_type E_end=E_begin+1; //SE

	   	const_it_state_type Is_begin=E_end; 
		const_it_state_type Is_end=Is_begin+1; //SEIs

	  	const_it_state_type Ia_begin=Is_end; 
		const_it_state_type Ia_end=Ia_begin+1; //SEIsIa

		const_it_state_type R_begin=Ia_end; 
		const_it_state_type R_end=R_begin+1; //SEIsIaR	

	    	double SuceptibleGroup=thrust::reduce(S_begin,S_end);
	    	double InfectedGroup_s=thrust::reduce(Is_begin,Is_end);	
	    	double InfectedGroup_a=thrust::reduce(Ia_begin,Ia_end);	
	    	double ExposedGroup=thrust::reduce(E_begin,E_end);
	    	double RecoveredGroup=thrust::reduce(R_begin,R_end);

		CumulatedInfectedGroup_s+=InfectedGroup_s*Dt;

	    	resout << t;
	    	resout << '\t' << SuceptibleGroup;
	    	resout << '\t' << ExposedGroup;
	    	resout << '\t' << InfectedGroup_s;
	    	resout << '\t' << InfectedGroup_a;
	    	resout << '\t' << RecoveredGroup;
		resout << '\t' << CumulatedInfectedGroup_s; 
		resout << endl;
	}
};


int naiveSIR(value_type BETA_S, value_type BETA_A, value_type ALPHA, value_type GAMMA, 
value_type FRAC, value_type MU, value_type NU, value_type trun)
{    	
    Dt=1;	 

    // initial conditions (S[0],I[0],R[0])
    state_type x(5,0.0); 
    value_type initialIs=0.001;	
    value_type initialS=1.0-initialIs;	
    x[2]=initialIs; 
    x[0]=initialS; 

    sir_system sir(x,BETA_S,BETA_A,ALPHA,GAMMA,FRAC,MU,NU,trun);

    // integracion 
    runge_kutta4< state_type , value_type , state_type , value_type > stepper;
    integrate_const( stepper, sir , x , 0.0 , trun , Dt , write_sir_detailed());
    //integrate( sir , x , 0.0 , trun , Dt , write_sir(NN,MM) );
 
    std::cout << sir.R0() << std::endl;	

    return 0;
}

int main(int argc, char **argv)
{
    assert(argc==9);
    value_type BETA_S=atof(argv[1]);
    value_type BETA_A=atof(argv[2]);
    value_type ALPHA=atof(argv[3]);
    value_type GAMMA=atof(argv[4]);
    value_type FRAC=atof(argv[5]);
    value_type MU=atof(argv[6]);
    value_type NU=atof(argv[7]);	
    value_type trun=atof(argv[8]);

    cpu_timer Reloj;

    #if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_OMP
    std::cout << "OMP run" << std::endl;	
    Reloj.tic();	
    #elif THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CPP
    std::cout << "CPP run" << std::endl;	
    Reloj.tic();	
    #elif THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
    std::cout << "CUDA run" << std::endl;	
    gpu_timer Reloj_gpu;
    Reloj_gpu.tic();	
    #else	
    #endif	    	
 
    naiveSIR(BETA_S,BETA_A,ALPHA,GAMMA,FRAC,MU,NU,trun);    	

    std::cout << "(cpu clock) ms=" << Reloj.tac() << std::endl;

    #if THRUST_DEVICE_SYSTEM==THRUST_DEVICE_SYSTEM_CUDA
    std::cout << "(gpu clock) ms=" << Reloj_gpu.tac() << std::endl;
    #endif
		
    return 0;
}

